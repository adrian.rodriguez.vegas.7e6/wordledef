import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class Main1KtTest{
    @Test
    fun checkPalabraAleatoria(){
        assertEquals("palabras", palabraAleatoria("catalan"))
    }
    @Test
    fun checkNombreUsuario(){
        assertEquals("Adri", nombreUsuario())
    }
    @Test
    fun checkDatosAlmacenados(){
        assertEquals(5, datosAlmacenados(0, 0, 0.0, 0.0, 0, 0))
    }
    @Test
    fun checkEscogerIidioma(){
        assertEquals("catalan", escogerIdioma())
        assertEquals("castellano", escogerIdioma())
    }
    @Test
    fun checkOpcionEstadistica(){
        assertEquals(5, opcionEstadistica(0,0,0.0,0,0,0))
    }
}
