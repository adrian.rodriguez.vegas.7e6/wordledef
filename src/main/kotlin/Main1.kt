import java.io.File

/*
        App: Wordle
        Description: Aquest programa és un joc de paraules. Primerament es selecciona l'idioma en el que es vol jugar i depenent l'idioma escollit s'haurà d'endevinar una paraula. La paraula s'ha d'endivinar en 6 intents, i aquesta només tindrà 5 lletres. Una vegada introduida la paraula per l'usuari es mostraran les lletres que coincideixen amb la paraula generada aleatoriament. Si la lletra coincideix en la posició es pintarà de color verd, si aquesta no es troba en la seva posició es mostrà de color groc i si la lletra no es troba en la paraula es pintarà de color gris. Finalment es mostren les estadistiques de les partides.
        Author: Adrián Rodríguez Vegas
        Date: 21/02/2023
*/


//Colores
val gris = "\u001b[48;2;120;124;126m"
val verde = "\u001b[42m"
val amarillo = "\u001b[43m"
val negro = "\u001b[40m"
val reset = "\u001b[0m"

//Variables generales
var rachaActual = 0
var mejorRacha = 0
var mediaIntentos = 0.0
var palabraAcertada = 0
var partidas = 0
var porcentVictoria = 0.0
var correcto: Boolean = false
var empezar = true

/**
 * Aquesta funció llegeix línia per línia l'arxiu seleccionat i ens en generarà una paraula aleatoria.
 * @author Adrián Rodríguez Vegas
 * @param idioma aquest parametre permet seleccionar entre els diferenst idiomas que ens dona a elegir, es un paremetre de tipus string.
 * @return ens retornarà una paraula aleatoria que es trobi dins del fitxer de l'idioma seleccionat.
 */
fun palabraAleatoria(idioma: String):String {
    val palabras = File("./src/main/kotlin/diccionarios/${idioma}.txt").readLines()
    return palabras.random()
}


/**
 * Funció que ens mostrarà el menú del joc, i ens donarà mes d'una opció.
 * @author Adrián Rodríguez Vegas
* */
fun menu1(){
    println("""
MENÚ:
        ||1. Jugar
        ||2. Estadisticas
        ||3. Salir
        """.trimMargin())
}

/**
 * És una funció que li demanarà el nom a l'usuari que jugarà la partida.
 * @author Adrián Rodríguez Vegas
 * @return la funció ens retorna un string, en aquest cas el nom de l'usuari.
 */
fun nombreUsuario():String{
    var nombre: String = readLine()!!.toString()
    return nombre
}

/**
 * Aquesta funció dona la benvinguda a l'usuari que jugarà.
 * @author Adrián Rodríguez Vegas
 */
fun introducirNombre(){
    print("Introduce tu nombre: ")
    val nombre = nombreUsuario()
    println("¡Bienvenido a Wordle $nombre!")
}

/**
 * Funció que ens mostra el joc i controlarà totes les característiques del joc, com els diferents intents dels que disposem, que les palaures obtinguin el color indicat en cada cas, i finalment controlar les estadístiques.
 * @author Adrián Rodríguez Vegas
 */
fun jugar (){
    var intentos = 0
    var posicion = 0
    val letras = arrayListOf<Char>()
    var idioma = escogerIdioma()

    var palabraGenerada = palabraAleatoria(idioma)
    println("""
            *******
            *$palabraGenerada*
            *******
            """.trimMargin())

    do {
        println("Adivina la palabra aleatoria, contiene máximo 5 letras. Dispones de 6 intentos para acertarla: ")
        var escribir = readLine()!!.toString()
        intentos++
        partidas++

        if (escribir == palabraGenerada){

            println(verde+escribir+reset)
            println("¡Palabra acertada!")

            palabraAcertada++

            if (correcto) {

                rachaActual++

                if (mejorRacha < rachaActual){
                    mejorRacha = rachaActual
                }

            }else {
                rachaActual = 0
            }
            break

        }else if (escribir != palabraGenerada){
            for (letra in escribir){

                if (palabraGenerada[posicion] == letra){
                    print(verde+letra+reset)
                    letras.add(letra)
                    palabraAcertada++

                }else if (palabraGenerada.contains(letra) && ! letras.contains(letra)){

                    print(amarillo+letra+reset)
                    letras.add(letra)

                } else{

                    print(gris+letra+reset)
                }
                posicion++
            }
            println()
        }
        porcentVictoria = (palabraAcertada.toDouble()/partidas.toDouble())*100
        mediaIntentos = (intentos/partidas).toDouble()

    }while (intentos != 6)
    estadisticas()
    opcionEstadistica(palabraAcertada, partidas, porcentVictoria, intentos, rachaActual, mejorRacha)
    datosAlmacenados(partidas, palabraAcertada, porcentVictoria, mediaIntentos, mejorRacha, rachaActual)
}

/**
 * Aquesta funció ens emmagatzemarà totes les dades de la partida
 * @author Adrián Rodríguez Vegas
 * @param partidas número de partides jugades
 * @param palabraAcertada número de paraules encertades
 * @param porcentVictoria percentatges de partides guanyades
 * @param mediaIntentos mitja d'intents per endevinar la paraula
 * @param mejorRacha millor ratxa de partides
 * @param rachaActual ratxa actual de la partida
 */
fun datosAlmacenados(partidas: Int, palabraAcertada: Int, porcentVictoria: Double, mediaIntentos: Double, mejorRacha: Int, rachaActual: Int){
    val userData = File("./src/main/kotlin/estadisticas/historial.txt")
    userData.appendText( """
            Partidas jugadas: $partidas
            Palabras acertadas: $palabraAcertada
            Porcentaje de victorias: $porcentVictoria
            Media de intentos: $mediaIntentos
            Mejor racha: $mejorRacha 
            Racha actual: $rachaActual
            """)
}

/**
 * És una funció que ens mostrarà un petit menú i ens permetrà escollir un idioma.
 * @author Adrián Rodríguez Vegas
 * @return ens retorna un parametre de tipus string, en aquest cas l'idioma seleccionat.
 */
fun escogerIdioma(): String {
    println(
        """
IDIOMA:
        || Castellano
        || Catalan
        """.trimMargin())
    print("Selecciona un idioma: ")
    var opcion = readLine()!!.toString()

    return opcion

}

/**
 * Creada per mostrar el menú de les estadístiques d'una partida.
 * @author Adrián Rodríguez Vegas
 */
fun estadisticas () {
    var menuEstadis = println(
        """
        ||1. Partidas
        ||2. Palabras acertadas
        ||3. Porcentaje de victorias
        ||4. Intentos
        ||5. Racha actual
        ||6. Mejor racha
        """.trimMargin()
    )
}

/**
 * Funció que ens preguntarà quina estadistica voledrem consultar i ens la mostrarà.
 * @author Adrián Rodríguez Vegas
 * @param partidas número de partides jugades
 * @param palabraAcertada número de paraules encertades
 * @param porcentVictoria percentatges de partides guanyades
 * @param mediaIntentos mitja d'intents per endevinar la paraula
 * @param mejorRacha millor ratxa de partides
 * @param rachaActual ratxa actual de la partida
 */
fun opcionEstadistica(palabraAcertada: Int, partidas: Int, porcentVictoria: Double, intentos: Int, rachaActual: Int, mejorRacha: Int){
    print("Selecciona una estadistica:")
    var opcEst: Int = readLine()!!.toInt()

    when (opcEst){
        1-> println("Has jugado un total de $partidas partidas. ")
        2-> println("Has acertado un total de $palabraAcertada palabras.")
        3-> println("Tienes un $porcentVictoria% de victorias.")
        4-> println("Lo has intentado $intentos veces. ")
        5-> println("La racha actual es de $rachaActual. ")
        6-> println("Tu mejor racha es de $mejorRacha.")
    }
}

/**
 * Funció que ens permetrà obtenir l'historial de les partides jugades, seleccionem l'arxiu desitjat i s'introduiran les estadístiques de la partida.
 * @author Adrián Rodríguez Vegas
 */
fun historialPartidas(){
    val file = File("./src/main/kotlin/estadisticas/historial.txt")
    println("Contingut inicial:")
    file.forEachLine { println(it) }
}

fun main() {
    var usuario = introducirNombre()

    do {
        menu1()
        print("Selecciona una opción: ")
        var opcion = readLine()!!.toInt()

        when (opcion) {
            1-> jugar()
            2-> historialPartidas()
            3-> println("¡Hasta luego!")
        }
    }while (opcion != 3)
}